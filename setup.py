import pathlib
from typing import List
from setuptools import setup, find_packages


class Setup:
    base_dir = pathlib.Path(__file__).parent.absolute()

    def __init__(self) -> None:
        self.version = self.get_version()
        self.long_desc = self.get_readme()
        self.packages = self.get_requires()
        self.license = self.get_license()

    def get_version(self) -> str:
        with open(self.base_dir / "VERSION") as file:
            return file.readline().strip()

    def get_readme(self) -> str:
        with open(self.base_dir / "README.md") as file:
            return file.read().strip()

    def get_requires(self) -> List[str]:
        with open(self.base_dir / "requirements.txt") as file:
            return [package.strip() for package in file.readlines() if package]

    def get_license(self):
        with open(self.base_dir / "LICENSE") as file:
            return file.read().strip()

    def __call__(self, name: str, author: str, email: str, url: str) -> None:
        version_line = '__version__ = "{}"\n'.format(self.version)
        init_file_path = self.base_dir / "src" / "fx" / "__init__.py"
        try:
            with open(init_file_path) as init_file:
                code = init_file.readlines()
        except FileNotFoundError:
            code = [version_line]
        else:
            import_position = None
            for index, line in enumerate(code):
                if "__version__" in line:
                    code[index] = version_line
                    break
                if "import" in line:
                    import_position = index
            else:
                if import_position is not None:
                    import_position += 1
                    code.insert(import_position, version_line)
                    code.insert(import_position, "\n\n")
                else:
                    code.insert(0, "\n\n")
                    code.insert(0, version_line)
        text = "".join(code).strip() + "\n"
        with open(init_file_path, "w") as init_file:
            init_file.write(text)
        setup(
            name=name,
            author=author,
            author_email=email,
            url=url,
            version=self.version,
            packages=find_packages("src", exclude=["*test*"]),
            package_dir={"": "src"},
            include_package_data=True,
            license=self.license,
            description="Forex robot",
            long_description=self.long_desc,
            long_description_content_type="text/markdown",
            install_requires=self.packages,
            python_requires=">=3.8",
            zip_safe=False,
            # entry_points = {"console_scripts": ["daam = adm.app"]},
            classifiers=[
                "Development Status :: 3 - Alpha"
                if "dev" in self.version
                else "Development Status :: 4 Beta"
                if "rc" in self.version
                else "Development Status :: 5 Production/Stable"
            ],
        )


Setup()(
    "fx-robot",
    "Dmitriy Amelchenko",
    "dima.amelchenko@live.com",
    "https://gitlab.com/xolloo/fxparser",
)
