LSE = {
    "container": "div.news__archive",
    "paginator": "div.pager",
    "header": "body > div.top-container > div.wrapper-3-col > div > div > div.wrapper-3-col__center > h1",
    "date": "body > div.top-container > div.wrapper-3-col > div > div > div.wrapper-3-col__center > div.news-article > div.news-article__details > p",
    "content": "body > div.top-container > div.wrapper-3-col > div > div > div.wrapper-3-col__center > div.news-article > div.news-article__content"
}