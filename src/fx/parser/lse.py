import asyncio
import inspect
import re
from importlib import reload
from datetime import datetime
from requests_html import HTML
from aiohttp import ClientSession
from aiohttp.web import HTTPOk
from tortoise import Model, fields, signals
from random import random

from ._selectors import LSE


class LseLinks:
    _url = "https://www.lse.co.uk/news/archive.html?page={}"

    def __init__(self, num_page):
        self._num_page = num_page
        self._links = []
        self._lse = None
        self._selectors = LSE

    async def load_data(self):
        async with ClientSession() as request:
            async with request.get(self._url.format(self._num_page)) as response:
                if response.status == HTTPOk.status_code:
                    data = await response.text()
                else:
                    raise ValueError(f"Page '{self._num_page}' not found.")
        self._lse = HTML(html=data)

    def _is_actual_page(self):
        page_box = self._lse.find(self._selectors["paginator"], first=True)
        page_link = page_box.find("span", first=True)
        return self._num_page == int(page_link.text)

    def process_data(self):
        if self._lse is not None and self._is_actual_page():
            container = self._lse.find(self._selectors["container"], first=True)
            # TODO: get all related links "https://www.lse.co.uk/news/HSBA/"
            self._links = [link.replace("\n", "") for link in container.absolute_links]
        else:
            raise ValueError(f"Links for page '{self._num_page}' not found.")

    @property
    def links(self):
        return self._links


class LseNews:
    _pattern = re.compile(r"[rdthsn]+")

    def __init__(self, url):
        self._url = url
        self._news = None
        self._data = {"url": self._url}
        self._selectors = LSE

    async def load_data(self):
        async with ClientSession() as request:
            async with request.get(self._url) as response:
                if response.status == HTTPOk.status_code:
                    data = await response.text()
                else:
                    raise ValueError(
                        f"Url '{self._url}' was returned '{response.status}' code."
                    )
        self._news = HTML(html=data)

    def process_data(self):
        if self._news is not None:
            h1 = self._news.find(self._selectors["header"], first=True)
            date = self._news.find(self._selectors["date"], first=True)
            content = self._news.find(self._selectors["content"], first=True)
            date = date.text.split(" ")[1:]
            date[0] = self._pattern.sub("", date[0])
            date = " ".join(date)
            self._data.update(
                title=h1.text,
                date=datetime.strptime(date, "%d %b %Y %H:%M"),
                content=content.text,
            )
        else:
            raise RuntimeError("News not found.")

    @property
    def news(self):
        return self._data

    @property
    def url(self):
        return self._url


class LinksLseModel(Model):
    id = fields.IntField(pk=True)
    link = fields.CharField(max_length=256)
    to_news = fields.BooleanField(default=True)

    class Meta:
        table = "links"

    def __str__(self) -> str:
        return str(self.link[:20])


@signals.pre_save(LinksLseModel)
async def check_link(sender, instance, using_db, update_fields):
    if instance.link.endswith("/"):
        instance.to_news = False


class NewsLseModel(Model):
    id = fields.IntField(pk=True)
    link = fields.OneToOneField(
        "parser.LinksLseModel",
        related_name="news",
    )
    title = fields.CharField(max_length=256)
    date = fields.DatetimeField()
    content = fields.TextField()

    class Meta:
        table = "news"

    def __str__(self) -> str:
        return str(self.title)


@signals.pre_save(NewsLseModel)
async def check_title_len(sender, instance, using_db, update_fields):
    if len(instance.title) > 256:
        instance.title = instance.title[:256]
