import json
import asyncio
import logging
import traceback
from dataclasses import dataclass
from asyncpg.exceptions import TooManyConnectionsError
from aiohttp.client_exceptions import ServerDisconnectedError, ClientOSError
from fx.config import settings
from fx.parser.lse import (
    LseLinks,
    LseNews,
    LinksLseModel,
    NewsLseModel,
)


@dataclass(eq=False)
class SaveLseLinks:

    start_page: int = 1
    end_page: int = 32025
    step = 100
    log = logging.getLogger("fx.dev" if settings.APP.debug else "fx.prod")
    except_log = logging.getLogger("fx.except")
    counter = 0

    async def __call__(self):
        self.counter = 0
        start_page = self.start_page if self.start_page < self.end_page else self.end_page
        if self.start_page > self.end_page:
            self.start_page = 1
            return
        pages = list(range(start_page, start_page + self.step))
        objects = [LseLinks(page) for page in pages]
        try:
            # tasks = asyncio.gather(*[obj.load_data() for obj in objects])
            async with asyncio.Semaphore(10):
                await asyncio.gather(*[obj.load_data() for obj in objects])
        except ServerDisconnectedError as error:
            self.log.error(error.args[0])
            self.except_log.error(traceback.format_exc())
            return
        except Exception as error:
            self.log.error(f"Page not found. {error.args[0]}.")
            self.except_log.error(traceback.format_exc())
            self.start_page += 1
            return
        errors = []
        for i, obj in enumerate(objects):
            try:
                obj.process_data()
            except ValueError as error:
                self.log.error(error.args[0])
                errors.append(i)
        if errors:
            for err_index in errors[::-1]:
                del objects[err_index]
        self.start_page += self.step

        for obj in objects:
            tasks = asyncio.gather(*map(self.save_links, obj.links))
            try:
                # async with asyncio.Semaphore(30):
                await tasks
            except Exception as error:
                self.log.error(f"Error save link. {error.args[0]}")
                self.except_log.error(traceback.format_exc())

        if self.counter and self.start_page == 1:
            self.log.info("Stop link parser.")
        self.log.debug(f"Start page. {self.start_page}.")

    async def save_links(self, link):
        if not await LinksLseModel.filter(link=link).count():
            return await LinksLseModel.create(link=link)
        if not link.endswith("/"):
            self.log.info(f"Link already exists. {link}.")
            self.counter += 1
        if self.counter >= self.step // 4:
            self.start_page = 1


class SaveLseNews:
    count = 100
    log = logging.getLogger("fx.dev" if settings.APP.debug else "fx.prod")
    except_log = logging.getLogger("fx.except")

    async def __call__(self):
        links = (
            await LinksLseModel.filter(to_news=True, news=None)
            .order_by("-id")
            .limit(self.count)
        )
        objects = [LseNews(url.link) for url in links]
        working = True
        while working:
            try:
                # tasks = asyncio.gather(*[obj.load_data() for obj in objects])
                async with asyncio.Semaphore(10):
                    await asyncio.gather(*[obj.load_data() for obj in objects])
            except ServerDisconnectedError as error:
                self.log.error(error.args[0])
                self.except_log.error(traceback.format_exc())
                await asyncio.sleep(60 * 20)
                return
            except ClientOSError as error:
                self.log.error(error.args[0])
                self.except_log.error(traceback.format_exc())
                return
            except Exception as error:
                # err_message = tasks.exception()
                self.log.error(error.args[0])
                for index, url in enumerate(links):
                    if url.link in error.args[0]:
                        del objects[index]
                        await url.delete()
                        del links[index]
                        if links:
                            break
                        else:
                            working = False
            else:
                working = False
        errors = []
        for index, obj in enumerate(objects):
            try:
                obj.process_data()
                self.log.info(f"Run process data for '{obj.url}',")
            except RuntimeError as error:
                errors.append(index)
                await LinksLseModel.get(link=obj.url).delete()
                self.log.error(error.args[0])
                self.except_log.error(traceback.format_exc())
        if errors:
            for index in errors[::-1]:
                del obj[index]
        news = [obj.news for obj in objects]
        tasks = asyncio.gather(*map(self.save_news, news))
        try:
            # async with asyncio.Semaphore(30):
            await tasks
        except Exception as error:
            self.log.error(error.args[0])
            self.except_log.error(traceback.format_exc())

    async def save_news(self, news):
        self.log.info(f"Save news for '{news['url']}'.")
        try:
            return await NewsLseModel.create(
                title=news["title"],
                date=news["date"],
                content=news["content"],
                link=await LinksLseModel.get(link=news["url"]),
            )
        except TooManyConnectionsError as error:
            self.log.error(error.args[0])
            self.except_log.error(traceback.format_exc())
        except Exception as error:
            self.log.error(error.args[0])
            self.except_log.error(traceback.format_exc())
            await LinksLseModel.get(link=news["url"]).delete()
