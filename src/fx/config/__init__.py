from dynaconf import settings


DB_CONFIG = {
    "connections": {
        "default": {
            "engine": "tortoise.backends.asyncpg",
            "credentials": {
                "host": settings.APP.db_host,
                "port": settings.APP.db_port,
                "user": settings.AUTH.db_user,
                "password": settings.AUTH.db_pass,
                "database": settings.APP.db_name,
                "minsize": 50,
                "maxsize": 90 if settings.APP.debug else 190,
            },
        },
    },
    "apps": {
        "parser": {
            "models": ["fx.parser.lse"],
            "default_connection": "default",
        },
    },
}
