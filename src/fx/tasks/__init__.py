from fx.tools import Tasks, BgTask
from fx.config import settings
from fx.parser import SaveLseLinks, SaveLseNews


Tasks.add(BgTask(SaveLseLinks(), settings.APP.timeout))
Tasks.add(BgTask(SaveLseNews(), settings.APP.timeout // 200))
