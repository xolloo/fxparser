import fx
import argparse
from fx.config import settings
from fx.app import Application, start_up


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog="Forex robot",
        description=("News parser.\n"),
        add_help=False,
    )
    parser.add_argument(
        "-h",
        "--help",
        action="help",
        help="Print the message.",
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        help="Current version.",
        version=fx.__version__,
    )
    parser.add_argument(
        "-b",
        "--background",
        dest="bg_tacks",
        action="store_true",
        required=False,
        help="Run background tasks.",
    )
    parser.add_argument(
        "-s",
        "--socketfile",
        default=settings.APP.socketfile,
    )
    return parser


args = create_parser().parse_args()
app = Application(args.socketfile, args.bg_tacks)
app.start_up.extend([start_up.init_logging, start_up.db])
app.start()
