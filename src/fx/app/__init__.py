import json
import uvloop
import asyncio
import importlib
from typing import Callable, List
from dataclasses import dataclass, field
from fx.tools import Tasks

loop = uvloop.new_event_loop()
asyncio.set_event_loop(loop)


class Command:
    request = None
    args = None

    async def get(self):
        print(self.request)
        print(self.args)
        return {"data": "result"}


class Controller(Command):
    async def __call__(self, reader, writer):
        data = await reader.read(2048)
        try:
            data = json.loads(data.decode("utf-8"))
        except json.decoder.JSONDecodeError:
            writer.close()
            return
        else:
            # TODO: сделать request объектом
            self.request = data["body"]
        handler = getattr(self, data.get("command"))
        if data.get("args"):
            # TODO: сделать args объектом
            self.args = data["args"]
        res = await handler(self)
        # TODO: сделать response объектом
        res = json.dumps(res).encode("utf-8")
        writer.write(res)
        await writer.drain()
        writer.close()
        reader.close()


@dataclass(eq=False, frozen=False)
class Application:
    socket_file: str
    # pid_file: str
    bg_tasks: bool
    server: object = field(init=False, default=None)
    controller: object = field(init=False)
    _start_up: List[Callable] = field(init=False, default_factory=list, repr=False)
    _tasks: List = field(default_factory=list, repr=False)
    _wait: bool = field(default=True, repr=False)
    _iterators: List[Callable] = field(init=False, repr=False, default_factory=list)

    def __post_init__(self):
        self.controller = Controller()

    @property
    def start_up(self):
        return self._start_up

    async def run(self) -> None:
        for cb in self._start_up:
            it = cb(self).__aiter__()
            await it.__anext__()
            self._iterators.append(it)
        self.server = await asyncio.start_unix_server(
            self.controller,
            path=self.socket_file,
        )
        if self.bg_tasks:
            importlib.import_module("fx.tasks")
            for task in Tasks()():
                task.run()
                self._tasks.append(task)
        try:
            await self.server.serve_forever()
        except asyncio.CancelledError:
            pass
        except Exception:
            import traceback

            traceback.print_exc()
        if self.bg_tasks:
            for task in self._tasks:
                task.stop(self._wait)
        for it in self._iterators[::-1]:
            try:
                await it.__anext__()
            except StopAsyncIteration:
                pass
        await self.server.wait_closed()

    def start(self) -> None:
        try:
            asyncio.run(self.run())
        except KeyboardInterrupt:
            print("\r", end="")
            self.server.close()
