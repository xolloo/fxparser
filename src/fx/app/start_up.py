import os
import yaml
import pathlib
import logging.config
from tortoise import Tortoise
from fx.config import settings, DB_CONFIG


async def db(app):
    await Tortoise.init(config=DB_CONFIG)
    await Tortoise.generate_schemas()
    yield
    await Tortoise.close_connections()


async def init_logging(app):
    path = pathlib.Path(settings.APP.logging_config).absolute()
    conf = yaml.safe_load(open(path.as_posix(), "r"))
    try:
        logging.config.dictConfig(config=conf)
    except ValueError:
        os.mkdir(settings.APP.log_dir)
        logging.config.dictConfig(config=conf)
    yield
