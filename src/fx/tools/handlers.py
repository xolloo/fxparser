import json
import socket
from logging.handlers import SysLogHandler


class TBot:
    async def send_message():
        ...


class JsonSysLogHandler(SysLogHandler):
    def emit(self, record):
        msg = self.format(record)
        try:
            msg = json.dumps(json.loads(msg))
        except Exception:
            msg = json.dumps(
                {
                    "message": msg,
                    "notification": "JsonSysLogHandler",
                }
            )
        prio = "<{}>".format(
            self.encodePriority(
                self.facility,
                self.mapPriority(record.levelname),
            )
        )
        msg = (prio + msg).encode("utf-8")
        msg = prio + msg
        if self.unixsocket:
            try:
                self.socket.send(msg)
            except OSError:
                self.socket.close()
                self._connect_unixsocket(self.address)
                self.socket.send(msg)
        elif self.socktype == socket.SOCK_DGRAM:
            self.socket.sendto(msg, self.address)
        else:
            self.socket.sendall(msg)
