import asyncio
from typing import Callable, Dict, Tuple, Any, Set
from dataclasses import dataclass, field


__all__ = ("Tasks" "BgTask",)


@dataclass(unsafe_hash=True)
class BgTask:
    cb: Callable
    interval: int
    args: Tuple = None
    kwargs: Dict = None
    _loop: asyncio.BaseEventLoop = None
    _working: bool = True
    _task_event: asyncio.Event = field(init=False, repr=False)
    _bgtask: asyncio.Task = field(init=False, repr=False, default=None)

    def __post_init__(self):
        self._loop = asyncio.get_running_loop()
        self._task_event = asyncio.Event(loop=self._loop)

    async def _executer(self):
        while self._working:
            self._loop.call_later(self.interval, lambda e: e.set(), self._task_event)
            if self.args is not None and self.kwargs is not None:
                curutine = self.cb(*self.args, **self.kwargs)
            elif self.args is not None and self.kwargs is None:
                curutine = self.cb(*self.args)
            elif self.args is None and self.kwargs is not None:
                curutine = self.cb(**self.kwargs)
            else:
                curutine = self.cb()
            await curutine
            await self._task_event.wait()
            self._task_event.clear()
































































            

    def run(self) -> None:
        self._bgtask = self._loop.create_task(self._executer())

    def stop(self, wait: bool = True) -> None:
        try:
            if wait:
                self._working = False
            else:
                self._bgtask.cancel()
        except asyncio.CancelledError:
            pass
        return self._bgtask


class Tasks:
    __slots__ = ()
    _bg_tasks = set()

    @classmethod
    def add(cls, task):
        cls._bg_tasks.add(task)

    def __call__(self):
        return list(self._bg_tasks)
