from functools import wraps
from enum import IntEnum

__all__ = ("Event",)


class Event:
    __callbacks = {}
    __callbacks_args = {}

    class name(IntEnum):
        START = 0
        STOP = 1
        SAVE = 2
        ADD = 3
        DELETE = 4

    @classmethod
    def register(cls, name, args=True):
        def _decorator(callback):
            if args:
                if name not in cls.__callbacks_args:
                    cls.__callbacks_args[name] = set()
                cls.__callbacks_args[name].add(callback)
            else:
                if name not in cls.__callbacks:
                    cls.__callbacks[name] = set()
                cls.__callbacks[name].add(callback)
            return callback

        return _decorator

    @classmethod
    def origin(cls, name, post=True):
        def _decorator(func):
            @wraps(func)
            async def _exe(*args, **kwargs):
                if post:
                    res = await func(*args, **kwargs)
                    await cls._occurance(name, *args, **kwargs)
                    return res
                await cls._occurance(name, *args, **kwargs)
                return await func(*args, **kwargs)

            return _exe

        return _decorator

    @classmethod
    async def _occurance(cls, name, *args, **kwargs):
        for callback in cls.__callbacks[name]:
            await callback()
        for callback in cls.__callbacks_args[name]:
            await callback(*args, **kwargs)
