from pytest import fixture
from tortoise import Tortoise
from fx.config import settings



@fixture(autouse=True)
async def db():
    await Tortoise.init(
        config={
            "connections": {
                "default": "sqlite://:memory:",
            },
            "apps": {
                "parser": {
                    "models": ["fx.parser.lse"],
                    "default_connection": "default",
                },
            },
        }
    )
    await Tortoise.generate_schemas()
    yield
    await Tortoise.close_connections()
