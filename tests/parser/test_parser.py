from pytest import mark, raises

from fx.parser.lse import LseLinks, LseNews

from .data import num_page, num_page_neg


@mark.asyncio
@mark.lse
async def test_load_page(num_page):
    lse_list = LseLinks(num_page)
    await lse_list.load_data()
    lse_list.process_data()
    assert len(lse_list.links)

@mark.asyncio
@mark.lse
async def test_load_news(num_page):
    lse_list = LseLinks(num_page)
    await lse_list.load_data()
    lse_list.process_data()
    link = lse_list.links[0]
    lse_news = LseNews(link)
    await lse_news.load_data()
    lse_news.process_data()
    assert len(lse_news.news)


@mark.asyncio
@mark.lse
async def test_page_neg(num_page_neg):
    lse = LseLinks(num_page_neg)
    await lse.load_data()
    with raises(ValueError):
        lse.process_data()
