from pytest import fixture
from ._values import num_pages


@fixture(params=num_pages)
def num_page(request):
    return request.param

@fixture(params=num_pages)
def num_page_neg(request):
    return -request.param