from pytest import fixture
from fx.tools.events import Event


@Event.origin(Event.name.ADD)
async def _go_0(*args, **kwargs):
    ...


@Event.origin(Event.name.SAVE)
async def _go_1(*args, **kwargs):
    ...


@fixture(
    params=[
        [_go_0, ((3, 5), None)],
        [_go_0, ((1, 2, 3), {"key_1": "value_1", "key_2": "value_2"})],
    ]
)
def callback_data(request):
    return request.param


@fixture(
    params=[
        [_go_1, ((3, 5), None)],
        [_go_1, ((1, 2, 3), {"key_1": "value_1", "key_2": "value_2"})],
    ]
)
def callback_data_args(request):
    return request.param
