from typing import AnyStr
from pytest import mark
from fx.tools import Event
from .data import callback_data, callback_data_args


global_store = None
global_store_args = None


@Event.register(Event.name.ADD, args=False)
@Event.register(Event.name.SAVE, args=False)
async def exe_0():
    global global_store
    global_store = ()


@Event.register(Event.name.ADD)
@Event.register(Event.name.SAVE)
async def exe_1(*args, **kwargs):
    global global_store_args
    global_store_args = args, kwargs


@mark.asyncio
@mark.tools
async def test_events(callback_data):
    cb, data = callback_data
    args, kwargs = data
    if kwargs is None:
        await cb(*args)
        assert global_store == ()
    else:
        await cb(*args, **kwargs)
        assert global_store == ()


@mark.asyncio
@mark.tools
async def test_events_args(callback_data_args):
    cb, data = callback_data_args
    args, kwargs = data
    if kwargs is None:
        await cb(*args)
        assert global_store_args == (args, {})
    else:
        await cb(*args, **kwargs)
        assert global_store_args == (args, kwargs)
