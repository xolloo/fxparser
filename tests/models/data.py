import asyncio
from pytest import fixture


async def init_table(model):
    await asyncio.gather(
        model.create(link="https://domain.com/path/to/news.html"),
        model.create(link="https://domain.com/path/to/news/"),
        model.create(link="https://domain.com/path/to/"),
        model.create(link="https://domain.com/path/to/news2.html"),
    )


@fixture(
    params=[
        "https://domain.com/path/to/news.html",
        "https://domain.com/path/to/news/",
        "https://domain.com/path/to/",
        "https://domain.com/path/to/news2.html",
    ]
)
def links(request):
    return request.param


@fixture(
    params=[
        ("https://domain.com/path/to/news.html", True),
        ("https://domain.com/path/to/news/", False),
        ("https://domain.com/path/to/", False),
        ("https://domain.com/path/to/news2.html", True),
    ]
)
def links_type(request):
    return request.param



