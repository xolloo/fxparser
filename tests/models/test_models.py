from types import new_class
from pytest import mark
from datetime import datetime
from fx.parser.lse import LinksLseModel, NewsLseModel

from .data import (
    links,
    links_type,
    init_table,
)


@mark.asyncio
@mark.models
async def test_check_link_type(links_type):
    l_text, t_type = links_type
    link = LinksLseModel()
    link.link = l_text
    await link.save()
    link = await LinksLseModel.get(id=link.id)
    assert link.to_news == t_type


@mark.asyncio
@mark.models
async def test_find_link(links):
    await init_table(LinksLseModel)
    count = await LinksLseModel.filter(link=links).count()
    assert count


from tortoise.query_utils import Prefetch


@mark.asyncio
@mark.models
@mark.single
async def test_find_link_without_news():
    await LinksLseModel.create(
        link=(
            "https://www.lse.co.uk/news/google-says-supports-work-"
            "to-update-international-tax-rules--fahdwqlzlypyjxk.html"
        )
    )
    await LinksLseModel.create(
        link=(
            "https://www.lse.co.uk/news/g7-tax-agreement-provides-"
            "level-playing-field-uks-sunak-says-afw7yrz1ve7p7tv.html"
        )
    )
    await LinksLseModel.create(
        link=(
            "https://www.lse.co.uk/news/live-markets-movies-and-"
            "video-games-may-lead-to-weed-ecz4d8buyftbqj9/"
        )
    )
    link = await LinksLseModel.get(id=2)
    news = await NewsLseModel.create(
        link=await LinksLseModel.get(id=2),
        title="kdkdkd",
        date=datetime.now(),
        content="kdkdkdkkdkdkdkd",
    )
    links = await LinksLseModel.filter(to_news=True, news=None)
    for link in links:
        assert link.id != news.link.id
